# RD-Dog

A LINE bot for IQS R&Ds

## Launch

```
git clone https://gitlab.com/zaplib/rd-dog.git
```

### SSL 

Prepare your ssl certificate and add them into `/cert` foler:

* `certificate.crt`: SSL certificates are used to create an encrypted channel between the client and the server.
* `private.key`: Private key.

> Note: LINE Webhook URL must use HTTPS and have an SSL/TLS certificate issued by a certificate authority widely trusted by general web browsers.  
> More info: [Set a Webhook URL](https://developers.line.biz/en/docs/messaging-api/building-bot/#setting-webhook-url)

### Installing required packages

```bash
pip install -r requirements.txt
```

#### Regenerate `requirements.txt`

```bash
pipreqs . --encoding=utf8 --force
```

### LINE Channel

Assuming you have already applied for LINE Channel, please set the necessary parameters.  
you can create `LINE.ini` file or set up evernote (choose one):

#### environment variables

* `ChannelID`
* `ChannelSecret`
* `ChannelAccessToken`

#### `/rd-dog/LINE.ini`

```ini
[Channel]
ChannelID = ""
ChannelSecret = ""
ChannelAccessToken = ""
```


### Dev-mode Server

starting app

#### environment variables

* `debug`: `True` (default) or `False`
* `PORT`: `443` (default) or other

#### cmd

```bash
set debug="True" && set port=443 && py /path/to/rd-dog/__main__.py
```

#### powershell

```powershell
($env:debug="True") -and ($env:port="443") -and (py /path/to/rd-dog/__main__.py) 
```

> **Cannot run it in PowerShell?**  
> you can install [powershell 7.0.6](https://github.com/PowerShell/PowerShell#windows-powershell-vs-powershell-core) (or higher)


### Production Server

Please see [Deploy to Production](https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/)


## Requirements

* Python 3.9.1 (or higher)
* pip 21.0.1 (or higher)

## LINCENSE

MIT.  
Copyright (c) 2021 ZapLin
