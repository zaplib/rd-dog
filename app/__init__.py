# import sys
# import ssl
import pathlib
import os
from flask import Flask
import pathlib
from .routers import append_routers

app = Flask(__name__)

current_dir = pathlib.Path(__file__).parent.absolute()
certpath = os.path.join(current_dir, '../cert/certificate.crt')
keypath = os.path.join(current_dir, '../cert/private.key')
capath = os.path.join(current_dir, '../cert/ca_bundle.crt')

append_routers(app)

port = os.environ.get("port", 443)
debug = os.environ.get("debug", "True").lower() in [
    'true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly']

# ssl
# ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
# ssl_context.verify_mode = ssl.CERT_REQUIRED
# ssl_context.load_verify_locations(capath)
# ssl_context.load_cert_chain(certpath, keypath)
ssl_context = (certpath, keypath)

app.run(host='0.0.0.0',
        debug=debug,
        port=port,
        ssl_context=ssl_context,
        threaded=True)
