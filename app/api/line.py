import os
import pathlib
import configparser

# get value from env or LINE.ini
# if can not get val by key, return None
def get_config(key):
    val = os.environ.get(key, None)
    if val is not None:
        return val.replace('"', '')
    if 'Channel' in config:
        channel = config['Channel']
        if key in channel:
            return channel[key].replace('"', '')
    return None


# read ini file
current_dir = pathlib.Path(__file__).parent.absolute()
inipath = os.path.join(current_dir, '../../LINE.ini')
ini_exists = os.path.isfile(inipath)
config = configparser.ConfigParser()
if ini_exists:
    config.read(inipath)


ChannelID = get_config('ChannelID')
ChannelSecret = get_config('ChannelSecret')
ChannelAccessToken = get_config('ChannelAccessToken')

print(ChannelID, ChannelSecret, ChannelAccessToken)