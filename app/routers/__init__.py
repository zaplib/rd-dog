from .root import main as root
from .line import main as line

def append_routers(app):
    root(app)
    line(app)