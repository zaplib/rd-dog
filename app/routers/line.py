
from flask import request
import sys


def main(app):
    @app.route("/line", methods=['GET', 'POST'])
    def line():
        if request.method == 'POST':
            # print(request.json, file=sys.stderr)
            json = request.get_json()
            events = json.get('events', None)
            # print(events, file=sys.stderr)
            for msg in parse_line_msg(events):
                print(msg, file=sys.stderr)
            return request.json
        return "ok"


def parse_line_msg(line_events):

    if line_events is None:
        return
    for msg in line_events:

        if 'replyToken' not in msg or 'source' not in msg or 'message' not in msg:
            continue

        if 'type' not in msg['message']: 
            continue

        if msg['message']['type'] != "text":
            continue
       
        if 'userId' not in msg['source'] or 'text' not in msg['message']:
            continue

        replyToken = msg['replyToken']
        source = msg['source']['userId']
        msg_txt = msg['message']['text']

        yield (replyToken, source, msg_txt)